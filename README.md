LibertyCodeModule_Error
=======================



Description
-----------

Library contains framework modules, 
to manage application errors.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code_module/error ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code_module/error": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Application installation
------------------------

#### Configuration

1. Configuration: application module: "<project root path>/config/Module.<config_file_ext>"

    Add in list part, required modules:

    Example for YML configuration format, from composer installation:

    ```yml
    list: [
        {
            path: "/vendor/liberty_code_module/error/src/error",
            config_parser: {
                type: "string_table_php",
                source_format_get_regexp: "#^\\<\\?php\\s*(.*)(\\s\\?\\>)?\\s*$#ms",
                source_format_set_pattern: "<?php \\n%1$s"
            }
        },
        {
            path: "/vendor/liberty_code_module/error/src/handler",
            config_parser: {
                type: "string_table_php",
                source_format_get_regexp: "#^\\<\\?php\\s*(.*)(\\s\\?\\>)?\\s*$#ms",
                source_format_set_pattern: "<?php \\n%1$s"
            }
        }
    ]
    ```

---



Configuration
-------------

#### Application parameters configuration

- Use following file on your modules to configure specific elements
    
    ```sh
    <module_root_path>/config/ParamApp.php
    ```

- Elements configurables

    - Configuration to param warning.
    
    - Configuration to populate handler collection

---



Usage
-----

#### Error

Features allow to set and manage error handling, available in application.

#### Handler

Features allow to set and manage basic handlers, available in application.

---


