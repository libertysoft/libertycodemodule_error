<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\error\error\di\build\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\di\dependency\api\DependencyCollectionInterface;
use liberty_code\di\dependency\preference\model\Preference;
use liberty_code\error\warning\tryer\model\DefaultWarnTryer;
use liberty_code\framework\application\api\AppInterface;



class ToolBoxBuilder extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate dependency collection,
     * from specified application object.
     *
     * @param DependencyCollectionInterface $objDependencyCollection
     * @param AppInterface $objApp
     */
    public static function hydrateDependencyCollection(
        DependencyCollectionInterface $objDependencyCollection,
        AppInterface $objApp
    )
    {
        // Init var
        $objExecTryer = $objApp->getObjExecTryer();
        $objSysTryer = $objApp->getObjSysTryer();
        $objWarnRegister = (
            ($objExecTryer instanceof DefaultWarnTryer) ?
                $objExecTryer->getObjWarnRegister() :
                (
                    ($objSysTryer instanceof DefaultWarnTryer) ?
                        $objSysTryer->getObjWarnRegister() :
                        null
                )
        );

        // Set warning register, on dependency, if required
        if(!is_null($objWarnRegister))
        {
            $objPref = new Preference(array(
                'key' => 'error_warning_register',
                'source' => get_class($objWarnRegister),
                'set' =>  ['type' => 'instance', 'value' => $objWarnRegister],
                'option' => [
                    //'shared' => true
                ]
            ));
            $objDependencyCollection->setDependency($objPref);
        }
    }
	
	
	
}