<?php

use liberty_code_module\error\error\boot\ErrorDiBootstrap;
use liberty_code_module\error\error\boot\ErrorBootstrap;



return array(
    'error_di_bootstrap' => [
        'call' => [
            'class_path_pattern' => ErrorDiBootstrap::class . ':boot'
        ]
    ],
    'error_bootstrap' => [
        'call' => [
            'class_path_pattern' => ErrorBootstrap::class . ':boot'
        ]
    ]
);