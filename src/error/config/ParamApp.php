<?php

use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\error\build\model\DefaultBuilder;
use liberty_code\error\build\directory\model\DirBuilder;
use liberty_code\error\warning\build\model\DefaultBuilder as WarnBuilder;
use liberty_code\error\warning\build\directory\model\DirBuilder as WarnDirBuilder;



return array(
    // Error configuration
    // ******************************************************************************

    'error' => [
        // Warning configuration
        'warning' => [
            'register' => [
                /**
                 * Configuration array format:
                 * @see DefaultTableRegister configuration format.
                 */
                'config' => []
            ]
        ],

        // Application execution configuration
        'execution' => [
            'handler' => [
                /**
                 * Configuration used by default builder,
                 * to populate handler collection,
                 * used during application execution.
                 *
                 * Format: @see DefaultBuilder data source format
                 */
                'config' => [],

                /**
                 * Configuration used by directory builder,
                 * to populate handler collection,
                 * used during application execution.
                 *
                 * Format: @see DirBuilder data source format
                 */
                'dir_path' => []
            ],

            'warning' => [
                'handler' => [
                    /**
                     * Configuration used by default builder,
                     * to populate warning handler collection,
                     * used during application execution.
                     *
                     * Format: @see WarnBuilder data source format
                     */
                    'config' => [],

                    /**
                     * Configuration used by directory builder,
                     * to populate warning handler collection,
                     * used during application execution.
                     *
                     * Format: @see WarnDirBuilder data source format
                     */
                    'dir_path' => []
                ]
            ]
        ],

        // Application system running configuration
        'system' => [
            'handler' => [
                /**
                 * Configuration used by default builder,
                 * to populate handler collection,
                 * used during application system running.
                 *
                 * Format: @see DefaultBuilder data source format
                 */
                'config' => [],

                /**
                 * Configuration used by directory builder,
                 * to populate handler collection,
                 * used during application system running.
                 *
                 * Format: @see DirBuilder data source format
                 */
                'dir_path' => []
            ],

            'warning' => [
                'handler' => [
                    /**
                     * Configuration used by default builder,
                     * to populate warning handler collection,
                     * used during application system running.
                     *
                     * Format: @see WarnBuilder data source format
                     */
                    'config' => [],

                    /**
                     * Configuration used by directory builder,
                     * to populate warning handler collection,
                     * used during application system running.
                     *
                     * Format: @see WarnDirBuilder data source format
                     */
                    'dir_path' => []
                ]
            ]
        ]
    ]
);