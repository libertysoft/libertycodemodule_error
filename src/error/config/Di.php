<?php

use liberty_code\config\config\api\ConfigInterface;
use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\error\handler\factory\api\HandlerFactoryInterface;
use liberty_code\error\build\model\DefaultBuilder;
use liberty_code\error\build\directory\model\DirBuilder;
use liberty_code\error\warning\handler\factory\api\WarnHandlerFactoryInterface;
use liberty_code\error\warning\build\model\DefaultBuilder as WarnBuilder;
use liberty_code\error\warning\build\directory\model\DirBuilder as WarnDirBuilder;
use liberty_code_module\error\error\tryer\build\model\TryerBuilder;
use liberty_code_module\error\error\warning\tryer\build\model\WarnTryerBuilder;
use liberty_code_module\error\error\warning\helper\WarningHelper;



return array(
    // Error handler services
    // ******************************************************************************

    'error_handler_builder' => [
        'source' => DefaultBuilder::class,
        'argument' => [
            ['type' => 'class', 'value' => HandlerFactoryInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'error_handler_directory_builder' => [
        'source' => DirBuilder::class,
        'argument' => [
            ['type' => 'class', 'value' => HandlerFactoryInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Error tryer services
    // ******************************************************************************

    'error_tryer_builder' => [
        'source' => TryerBuilder::class,
        'argument' => [
            ['type' => 'class', 'value' => ConfigInterface::class],
            ['type' => 'dependency', 'value' => 'error_handler_builder'],
            ['type' => 'dependency', 'value' => 'error_handler_directory_builder']
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Error warning register services
    // ******************************************************************************

    // Register on boot, from application, if required
    'error_warning_register' => [
        'source' => DefaultTableRegister::class,
        'argument' => [
            ['type' => 'config', 'value' => 'error/warning/register/config'],
            ['type' => 'mixed', 'value' => null]
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Error warning handler services
    // ******************************************************************************

    'error_warning_handler_builder' => [
        'source' => WarnBuilder::class,
        'argument' => [
            ['type' => 'class', 'value' => WarnHandlerFactoryInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'error_warning_handler_directory_builder' => [
        'source' => WarnDirBuilder::class,
        'argument' => [
            ['type' => 'class', 'value' => WarnHandlerFactoryInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Error warning tryer services
    // ******************************************************************************

    'error_warning_tryer_builder' => [
        'source' => WarnTryerBuilder::class,
        'argument' => [
            ['type' => 'class', 'value' => ConfigInterface::class],
            ['type' => 'dependency', 'value' => 'error_warning_handler_builder'],
            ['type' => 'dependency', 'value' => 'error_warning_handler_directory_builder'],
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Error warning helper services
    // ******************************************************************************

    'error_warning_helper' => [
        'source' => WarningHelper::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'error_warning_register']
        ],
        'option' => [
            'shared' => true
        ]
    ]
);