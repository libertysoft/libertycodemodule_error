<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\error\error\boot;

use liberty_code\library\instance\model\Multiton;

use liberty_code\framework\module\api\ModuleInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\bootstrap\library\ToolBoxBootstrap;
use liberty_code\framework\framework\config\library\ToolBoxConfig;



class ToolBoxErrorBootstrap extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods setters
	// ******************************************************************************

	/**
     * Put error configuration,
     * from specified configuration full file path.
     *
     * @param AppInterface $objApp
     * @param ModuleInterface $objModule
     * @param string $strFilePath
     * @return boolean
     */
    public static function putErrorConfigFile(
        AppInterface $objApp,
        ModuleInterface $objModule,
        $strFilePath
    )
    {
        // Init var
        $result = ToolBoxBootstrap::putAppConfigFile(
            $objApp,
            $objModule,
            $strFilePath,
            'error'
        );

        // Return result
        return $result;
    }



    /**
     * Add error directory full path.
     *
     * @param AppInterface $objApp
     * @param string $strDirPath
     * @param array $tabConfigKey
     * @return boolean
     */
    protected static function addErrorDirPath(
        AppInterface $objApp,
        $strDirPath,
        array $tabConfigKey
    )
    {
        // Init var
        $result = false;
        $objConfig = $objApp->getObjConfig();
        $strConfigKey = ToolBoxConfig::getStrPathKey($tabConfigKey);

        // Set directory path on config
        if(!is_null($objConfig))
        {
            // Init array of directory paths
            $tabDirPath = $objConfig->getValue($strConfigKey);
            $tabDirPath = (is_array($tabDirPath) ? $tabDirPath : array());

            // Register new directory path
            $tabDirPath[] = $strDirPath;

            // Reset array of directory paths on config
            $result = ToolBoxBootstrap::putAppConfigConfigValue(
                $objApp,
                $strConfigKey,
                $tabDirPath
            );
        }

        // Return result
        return $result;
    }



    /**
     * Add error handler directory full path.
     * used during application execution.
     *
     * @param AppInterface $objApp
     * @param string $strDirPath
     * @return boolean
     */
    public static function addErrorExecHandlerDirPath(
        AppInterface $objApp,
        $strDirPath
    )
    {
        // Return result
        return static::addErrorDirPath(
            $objApp,
            $strDirPath,
            array('error', 'execution', 'handler', 'dir_path')
        );
    }



    /**
     * Add error warning handler directory full path.
     * used during application execution.
     *
     * @param AppInterface $objApp
     * @param string $strDirPath
     * @return boolean
     */
    public static function addErrorExecWarnHandlerDirPath(
        AppInterface $objApp,
        $strDirPath
    )
    {
        // Return result
        return static::addErrorDirPath(
            $objApp,
            $strDirPath,
            array('error', 'execution', 'warning', 'handler', 'dir_path')
        );
    }



    /**
     * Add error handler directory full path.
     * used during application system running.
     *
     * @param AppInterface $objApp
     * @param string $strDirPath
     * @return boolean
     */
    public static function addErrorSysHandlerDirPath(
        AppInterface $objApp,
        $strDirPath
    )
    {
        // Return result
        return static::addErrorDirPath(
            $objApp,
            $strDirPath,
            array('error', 'system', 'handler', 'dir_path')
        );
    }



    /**
     * Add error warning handler directory full path.
     * used during application system running.
     *
     * @param AppInterface $objApp
     * @param string $strDirPath
     * @return boolean
     */
    public static function addErrorSysWarnHandlerDirPath(
        AppInterface $objApp,
        $strDirPath
    )
    {
        // Return result
        return static::addErrorDirPath(
            $objApp,
            $strDirPath,
            array('error', 'system', 'warning', 'handler', 'dir_path')
        );
    }



}