<?php
/**
 * Description :
 * This class allows to define error module bootstrap class.
 * Error module bootstrap allows to boot error module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\error\error\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\error\warning\tryer\api\WarnTryerInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code_module\error\error\library\ConstError;
use liberty_code_module\error\error\tryer\build\model\TryerBuilder;
use liberty_code_module\error\error\warning\tryer\build\model\WarnTryerBuilder;



class ErrorBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Tryer builder instance.
     * @var TryerBuilder
     */
    protected $objTryerBuilder;



    /**
     * DI: Warning tryer builder instance.
     * @var WarnTryerBuilder
     */
    protected $objWarnTryerBuilder;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param TryerBuilder $objTryerBuilder
     * @param WarnTryerBuilder $objWarnTryerBuilder
     */
    public function __construct(
        AppInterface $objApp,
        TryerBuilder $objTryerBuilder,
        WarnTryerBuilder $objWarnTryerBuilder
    )
    {
        // Init properties
        $this->objTryerBuilder = $objTryerBuilder;
        $this->objWarnTryerBuilder = $objWarnTryerBuilder;

        // Call parent constructor
        parent::__construct($objApp, ConstError::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Init var
        $objApp = $this->getObjApp();
        $objExecTryer = $objApp->getObjExecTryer();
        $objSysTryer = $objApp->getObjSysTryer();

        // Hydrate execution tryer, if required
        if(!is_null($objExecTryer))
        {
            $this->objTryerBuilder->hydrateExecTryer($objExecTryer);

            if($objExecTryer instanceof WarnTryerInterface)
            {
                $this->objWarnTryerBuilder->hydrateExecTryer($objExecTryer);
            }
        }

        // Hydrate system tryer, if required
        if(!is_null($objSysTryer))
        {
            $this->objTryerBuilder->hydrateSysTryer($objSysTryer);

            if($objSysTryer instanceof WarnTryerInterface)
            {
                $this->objWarnTryerBuilder->hydrateSysTryer($objSysTryer);
            }
        }
    }



}