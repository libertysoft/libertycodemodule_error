<?php
/**
 * Description :
 * This class allows to define error module DI (dependency injection) bootstrap class.
 * Error module DI bootstrap allows to boot error module DI.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\error\error\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\di\dependency\api\DependencyCollectionInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code_module\error\error\library\ConstError;
use liberty_code_module\error\error\di\build\library\ToolBoxBuilder;



class ErrorDiBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Dependency collection instance.
     * @var DependencyCollectionInterface
     */
    protected $objDependencyCollection;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DependencyCollectionInterface $objDependencyCollection
     */
    public function __construct(
        AppInterface $objApp,
        DependencyCollectionInterface $objDependencyCollection
    )
    {
        // Init properties
        $this->objDependencyCollection = $objDependencyCollection;

        // Call parent constructor
        parent::__construct($objApp, ConstError::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Init var
        $objApp = $this->getObjApp();

        // Hydrate dependency collection
        ToolBoxBuilder::hydrateDependencyCollection($this->objDependencyCollection, $objApp);
    }



}