<?php
/**
 * Description :
 * This class allows to define warning tryer builder class.
 * Warning tryer builder allows to populate warning tryer,
 * from configuration.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\error\error\warning\tryer\build\model;

use liberty_code\library\bean\model\DefaultBean;

use liberty_code\config\config\api\ConfigInterface;
use liberty_code\error\warning\build\model\DefaultBuilder;
use liberty_code\error\warning\build\directory\model\DirBuilder;
use liberty_code\error\warning\tryer\api\WarnTryerInterface;
use liberty_code\framework\framework\config\library\ToolBoxConfig;



class WarnTryerBuilder extends DefaultBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Application configuration instance.
     * @var ConfigInterface
     */
    protected $objAppConfig;



    /**
     * DI: Handler builder instance.
     * @var DefaultBuilder
     */
    protected $objBuilder;



    /**
     * DI: Handler directory builder instance.
     * @var DirBuilder
     */
    protected $objDirBuilder;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ConfigInterface $objAppConfig
     * @param DefaultBuilder $objBuilder
     * @param DirBuilder $objDirBuilder
     */
    public function __construct(
        ConfigInterface $objAppConfig,
        DefaultBuilder $objBuilder,
        DirBuilder $objDirBuilder
    )
    {
        // Init properties
        $this->objAppConfig = $objAppConfig;
        $this->objBuilder = $objBuilder;
        $this->objDirBuilder = $objDirBuilder;

        // Call parent constructor
        parent::__construct();
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified warning tryer,
     * from configuration.
     *
     * @param WarnTryerInterface $objTryer
     * @param string $strConfigKey
     */
    protected function hydrateWarnTryer(
        WarnTryerInterface $objTryer,
        $strConfigKey
    )
    {
        // Init var
        $objWarnHandlerCollection = $objTryer->getObjWarnHandlerCollection();

        // Hydrate builders
        $this->objDirBuilder->setTabDataSrc($this->getBuilderTabDataSrc(
            array('error', $strConfigKey, 'warning', 'handler', 'dir_path')
        ));
        $this->objBuilder->setTabDataSrc($this->getBuilderTabDataSrc(
            array('error', $strConfigKey, 'warning', 'handler', 'config')
        ));

        // Build warning handler collection
        $this->objDirBuilder->hydrateWarnHandlerCollection($objWarnHandlerCollection);
        $this->objBuilder->hydrateWarnHandlerCollection($objWarnHandlerCollection, false);
    }



    /**
     * Hydrate specified execution tryer,
     * from configuration.
     *
     * @param WarnTryerInterface $objTryer
     */
    public function hydrateExecTryer(WarnTryerInterface $objTryer)
    {
        $this->hydrateWarnTryer($objTryer, 'execution');
    }



    /**
     * Hydrate specified system tryer,
     * from configuration.
     *
     * @param WarnTryerInterface $objTryer
     */
    public function hydrateSysTryer(WarnTryerInterface $objTryer)
    {
        $this->hydrateWarnTryer($objTryer, 'system');
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get data source array,
     * for handler builder,
     * from specified configuration.
     *
     * @param array $tabConfigKey
     * @return array
     */
    protected function getBuilderTabDataSrc(array $tabConfigKey)
    {
        // Init var
        $strConfigKey = ToolBoxConfig::getStrPathKey($tabConfigKey);
        $result = (
            (
                $this->objAppConfig->checkValueExists($strConfigKey) &&
                is_array($this->objAppConfig->getValue($strConfigKey))
            ) ?
                $this->objAppConfig->getValue($strConfigKey) :
                array()
        );

        // Return result
        return $result;
    }
	
	
	
}