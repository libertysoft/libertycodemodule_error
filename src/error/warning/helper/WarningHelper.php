<?php
/**
 * Description :
 * This class allows to define warning helper class.
 * Warning helper allows to provide features, about warnings.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\error\error\warning\helper;

use liberty_code\library\bean\model\DefaultBean;

use Throwable;
use liberty_code\register\register\api\RegisterInterface;
use liberty_code\error\warning\library\ToolBoxWarnRegister;



class WarningHelper extends DefaultBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Warning register instance.
     * @var RegisterInterface
     */
    protected $objWarnRegister;




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor / Others
	// ******************************************************************************

	/**
	 * @inheritdoc
     * @param RegisterInterface $objWarnRegister
     */
	public function __construct(
        RegisterInterface $objWarnRegister
    )
	{
        // Init properties
        $this->objWarnRegister = $objWarnRegister;

		// Call parent constructor
		parent::__construct();
	}





    // Methods setters
    // ******************************************************************************

    /**
     * Put specified throwable,
     * on warning register.
     * Return string register key if success, false else.
     *
     * Configuration format:
     * @see ToolBoxWarnRegister::putThrowable() configuration format.
     *
     * @param Throwable $objThrowable
     * @param string $strKey = null
     * @param array $tabConfig = null
     * @return boolean|string
     */
    public function putThrowable(
        Throwable $objThrowable,
        $strKey = null,
        array $tabConfig = null
    )
    {
        // Return result
        return ToolBoxWarnRegister::putThrowable(
            $this->objWarnRegister,
            $objThrowable,
            $strKey,
            $tabConfig
        );
    }



    /**
     * Remove specified throwable,
     * from warning register.
     *
     * @param string|Throwable $throwable : string throwable key|throwable object
     * @return boolean
     */
    public function removeThrowable($throwable)
    {
        // Return result
        return ToolBoxWarnRegister::removeThrowable(
            $this->objWarnRegister,
            $throwable
        );
    }



}