<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\error\handler\library;

use liberty_code\library\instance\model\Multiton;

use Throwable;
use liberty_code\command\request_flow\response\info\library\ToolBoxInfoResponse;



class ToolBoxCommandRender extends Multiton
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    /**
     * Get string render,
     * from specified throwable object.
     *
     * @param Throwable $objThrowable
     * @param string $strThrowableType = 'error'
     * @return string
     */
    public static function getStrThrowableRender(
        Throwable $objThrowable,
        $strThrowableType = 'error'
    )
    {
        // Init var
        $strThrowableType = (is_string($strThrowableType) ? $strThrowableType : 'error');

        // Get render array
        $tabRender = array(
            [
                sprintf('Following %1$s found', $strThrowableType),
                ':' . get_class($objThrowable)
            ],
            [
                sprintf('Message (Code: %1$s)', $objThrowable->getCode()),
                ':' . $objThrowable->getMessage()
            ],
            ['From file', ':' . $objThrowable->getFile()],
            ['On line', ':' . $objThrowable->getLine()]
        );

        // Get trace render array
        $tabTraceRender = array(
            ['#', 'Function', 'Location'],
            ['-', '--------', '--------'],
        );
        $tabTrace = array_values(array_slice($objThrowable->getTrace(), 0, 10));
        for($intCpt = 0; $intCpt < count($tabTrace); $intCpt++)
        {
            // Get info
            $trace = $tabTrace[$intCpt];
            $strFunction = (
                isset($trace['function']) ?
                    (
                        isset($trace['class']) ?
                            sprintf(
                                '%1$s::%2$s( )',
                                $trace['class'],
                                $trace['function']
                            ) :
                            sprintf(
                                '%1$s( )',
                                $trace['function']
                            )
                    ) :
                    ' - '
            );
            $strLocation = (
                isset($trace['file']) ?
                    sprintf(
                        '%1$s:%2$s',
                        $trace['file'],
                        $trace['line']
                    ) :
                    ' - '
            );

            // Set trace render
            $tabTraceRender[] = array(
                $intCpt,
                $strFunction,
                $strLocation
            );
        }

        // Set result
        $result =
            ToolBoxInfoResponse::getStrTable($tabRender) . PHP_EOL .
            'Traces:' . PHP_EOL .
            ToolBoxInfoResponse::getStrTable($tabTraceRender);

        // Return result
        return $result;
    }



    /**
     * Get string error render,
     * from specified throwable object.
     *
     * @param Throwable $objThrowable
     * @param boolean $boolDetailRequired = false
     * @return string
     */
    public static function getStrErrorRender(
        Throwable $objThrowable,
        $boolDetailRequired = false
    )
    {
        // Init var
        $boolDetailRequired = (is_bool($boolDetailRequired) ? $boolDetailRequired : false);
        $result =
            'Error occurs' .
            (
                $boolDetailRequired ?
                    PHP_EOL . PHP_EOL . static::getStrThrowableRender($objThrowable) . PHP_EOL :
                    ''
            );

        // Return result
        return $result;
    }



    /**
     * Get string warning render,
     * from specified index array of throwable objects.
     *
     * @param Throwable[] $tabThrowable
     * @param boolean $boolDetailRequired = false
     * @return string
     */
    public static function getStrWarnRender(
        array $tabThrowable,
        $boolDetailRequired = false
    )
    {
        // Init var
        $boolDetailRequired = (is_bool($boolDetailRequired) ? $boolDetailRequired : false);
        $result =
            PHP_EOL . PHP_EOL .
            'Warnings occur';

        // Get warning details, if required
        if($boolDetailRequired)
        {
            foreach($tabThrowable as $objThrowable)
            {
                if($objThrowable instanceof Throwable)
                {
                    $result .=
                        PHP_EOL . PHP_EOL .
                        static::getStrThrowableRender($objThrowable, 'warning') .
                        PHP_EOL;
                }
            }
        }

        // Return result
        return $result;
    }



}