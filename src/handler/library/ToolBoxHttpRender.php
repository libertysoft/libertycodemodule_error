<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\error\handler\library;

use liberty_code\library\instance\model\Multiton;

use Throwable;
use liberty_code\library\reflection\library\ToolBoxClassReflection;



class ToolBoxHttpRender extends Multiton
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    /**
     * Get string render,
     * from specified throwable object.
     *
     * @param Throwable $objThrowable
     * @param string $strThrowableType = 'error'
     * @return string
     */
    public static function getStrThrowableRender(
        Throwable $objThrowable,
        $strThrowableType = 'error'
    )
    {
        // Init var
        $strThrowableType = (is_string($strThrowableType) ? $strThrowableType : 'error');
        $strThrowableClassPath = get_class($objThrowable);

        // Get trace render
        $strTraceRender = '';
        $tabTrace = array_values(array_slice($objThrowable->getTrace(), 0, 10));
        for($intCpt = 0; $intCpt < count($tabTrace); $intCpt++)
        {
            // Get info
            $trace = $tabTrace[$intCpt];
            $strFunction = (
                isset($trace['function']) ?
                    (
                        isset($trace['class']) ?
                            sprintf(
                                '%1$s::%2$s( )',
                                $trace['class'],
                                $trace['function']
                            ) :
                            sprintf(
                                '%1$s( )',
                                $trace['function']
                            )
                    ) :
                    ' - '
            );
            $strFunctionShort = ToolBoxClassReflection::getStrClassName($strFunction);
            $strFunctionShort =
                (($strFunctionShort != $strFunction) ? '...\\' : '') .
                $strFunctionShort;
            $strLocation = (
                isset($trace['file']) ?
                    sprintf(
                        '%1$s:%2$s',
                        $trace['file'],
                        $trace['line']
                    ) :
                    ' - '
            );
            $strLocationShort = basename($strLocation);
            $strLocationShort =
                (($strLocationShort != $strLocation) ? '.../' : '') .
                $strLocationShort;

            // Set trace render
            $strTraceRender .= PHP_EOL . <<<HTML
<tr>
    <td bgcolor="#eeeeec" align="center">$intCpt</td>
    <td title="$strFunction" bgcolor="#eeeeec">$strFunctionShort</td>
    <td title="$strLocation" bgcolor="#eeeeec">$strLocationShort</td>
</tr>
HTML;
        }

        // Set result
        $result = <<<HTML
<table class="xdebug-error xe-fatal-error" dir="ltr" border="1" cellspacing="0" cellpadding="1">
    <tbody>
        <tr>
            <th align="left" bgcolor="#f57900" colspan="5">
                <span style="background-color: #cc0000; color: #fce94f; font-size: x-large;">( ! )</span> 
                Following $strThrowableType found: $strThrowableClassPath <br />
                Message (Code {$objThrowable->getCode()}): {$objThrowable->getMessage()} <br />
                From file {$objThrowable->getFile()}, on line {$objThrowable->getLine()}
            </th>
        </tr>
        <tr>
            <th align="left" bgcolor="#e9b96e" colspan="5">
                Traces
            </th>
        </tr>
        <tr>
            <th align="center" bgcolor="#eeeeec">#</th>
            <th align="left" bgcolor="#eeeeec">Function</th>
            <th align="left" bgcolor="#eeeeec">Location</th>
        </tr>
        $strTraceRender
    </tbody>
</table>
HTML;

        // Return result
        return $result;
    }



    /**
     * Get string error render,
     * from specified throwable object.
     *
     * @param Throwable $objThrowable
     * @param boolean $boolDetailRequired = false
     * @return string
     */
    public static function getStrErrorRender(
        Throwable $objThrowable,
        $boolDetailRequired = false
    )
    {
        // Init var
        $boolDetailRequired = (is_bool($boolDetailRequired) ? $boolDetailRequired : false);
        $result =
            '<h3>Error occurs</h3>' .
            (
                $boolDetailRequired ?
                    '<br /><br />'. static::getStrThrowableRender($objThrowable) :
                    ''
            );

        // Return result
        return $result;
    }



    /**
     * Get string warning render,
     * from specified index array of throwable objects.
     *
     * @param Throwable[] $tabThrowable
     * @param boolean $boolDetailRequired = false
     * @return string
     */
    public static function getStrWarnRender(
        array $tabThrowable,
        $boolDetailRequired = false
    )
    {
        // Init var
        $boolDetailRequired = (is_bool($boolDetailRequired) ? $boolDetailRequired : false);
        $result =
            '<br /><br />'.
            '<h3>Warnings occur</h3>';

        // Get warning details, if required
        if($boolDetailRequired)
        {
            foreach($tabThrowable as $objThrowable)
            {
                if($objThrowable instanceof Throwable)
                {
                    $result .=
                        '<br /><br />' .
                        static::getStrThrowableRender($objThrowable, 'warning');
                }
            }
        }

        // Return result
        return $result;
    }



}