<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\error\handler\library;



class ConstHandler
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration module
    const MODULE_KEY = 'error_handler';
}