<?php
/**
 * Description :
 * Allows to define throwable handler class.
 * Throwable handler is fixed system throwable handler,
 * allows to handle all throwable errors by default.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\error\handler\error\system;

use liberty_code\framework\error\handler\throwable\fix\model\FixSysThrowableHandler;

use Throwable;
use liberty_code\register\register\api\RegisterInterface;
use liberty_code\request_flow\request\api\RequestInterface;
use liberty_code\request_flow\response\api\ResponseInterface;
use liberty_code\request_flow\response\model\DefaultResponse;
use liberty_code\http\request_flow\request\model\HttpRequest;
use liberty_code\http\request_flow\response\model\HttpResponse;
use liberty_code\framework\app_mode\config\library\ConstConfig;
use liberty_code\framework\app_mode\select\api\SelectorInterface;
use liberty_code_module\error\handler\error\execution\warning\ErrorThrowableWarnHandler;
use liberty_code_module\error\handler\library\ToolBoxHttpRender;
use liberty_code_module\error\handler\library\ToolBoxCommandRender;



class ThrowableHandler extends FixSysThrowableHandler
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Application mode selector instance.
     * @var SelectorInterface
     */
    protected $objAppMode;



    /**
     * DI: Request instance.
     * @var RequestInterface
     */
    protected $objRequest;



    /**
     * DI: Error throwable warning handler instance.
     * @var ErrorThrowableWarnHandler
     */
    protected $objErrorThrowableWarnHandler;


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param SelectorInterface $objAppMode
     * @param RequestInterface $objRequest
     * @param ErrorThrowableWarnHandler $objErrorThrowableWarnHandler
     */
    public function __construct(
        SelectorInterface $objAppMode,
        RequestInterface $objRequest,
        RegisterInterface $objWarnRegister,
        ErrorThrowableWarnHandler $objErrorThrowableWarnHandler
    )
    {
        // Init var
        $this->objAppMode = $objAppMode;
        $this->objRequest = $objRequest;
        $this->objErrorThrowableWarnHandler = $objErrorThrowableWarnHandler;

        // Call parent constructor
        parent::__construct($objWarnRegister);
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check render required.
     *
     * @return boolean
     */
    protected function checkRenderRequired()
    {
        // Return result
        return boolval($this->objAppMode->getActiveModeValue(ConstConfig::TAB_CONFIG_KEY_DEBUG));
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        return array();
    }



    /**
     * @inheritdoc
     * @param Throwable $error
     * @return DefaultResponse|HttpResponse
     */
    protected function getObjResponse($error)
    {
        // Get render
        $strRender = (
            ($this->objRequest instanceof HttpRequest) ?
                ToolBoxHttpRender::getStrErrorRender($error, $this->checkRenderRequired()) :
                ToolBoxCommandRender::getStrErrorRender($error, $this->checkRenderRequired())
        );

        // Set result: get response
        $result = new DefaultResponse();
        if($this->objRequest instanceof HttpRequest)
        {
            $result = new HttpResponse();
            $result->setIntStatusCode(500);
        }
        $result->setContent($strRender);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @param DefaultResponse|HttpResponse $objResponse
     */
    protected function getObjWarnResponse(ResponseInterface $objResponse)
    {
        // Init var
        $objWarnRegister = $this->getObjWarnRegister();
        $tabWarning = array_values($objWarnRegister->getTabItem());

        // Set result: get response, with error exception warnings render, if required
        $result = $this->objErrorThrowableWarnHandler->execute(
            $this->objErrorThrowableWarnHandler->getTabCatchWarning($tabWarning),
            $objResponse,
            false
        );

        // Return result
        return $result;
    }



}