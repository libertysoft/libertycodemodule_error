<?php
/**
 * Description :
 * Allows to define error handler class.
 * Error handler is fixed throw error handler,
 * allows to handle standard errors with level error, ... (fatal errors).
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\error\handler\error;

use liberty_code\error\handler\error\fix\model\FixThrowErrorHandler;

use liberty_code\error\handler\error\library\ConstErrorHandler;



class ErrorHandler extends FixThrowErrorHandler
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstErrorHandler::TAB_CONFIG_KEY_ERROR_TYPE_INCLUDE => [
                E_ERROR,
                E_PARSE,
                E_CORE_ERROR,
                E_COMPILE_ERROR,
                E_USER_ERROR,
                E_RECOVERABLE_ERROR
            ]
        );
    }



}