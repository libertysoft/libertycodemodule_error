<?php
/**
 * Description :
 * Allows to define error throwable warning handler class.
 * Error throwable warning handler is fixed execution throwable warning handler,
 * allows to handle all error exception warnings.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\error\handler\error\execution\warning;

use liberty_code\framework\error\warning\handler\throwable\fix\model\FixExecThrowableWarnHandler;

use ErrorException;
use liberty_code\request_flow\response\api\ResponseInterface;
use liberty_code\request_flow\response\model\DefaultResponse;
use liberty_code\http\request_flow\response\model\HttpResponse;
use liberty_code\error\warning\handler\throwable\library\ConstThrowableWarnHandler;
use liberty_code\framework\app_mode\config\library\ConstConfig;
use liberty_code\framework\app_mode\select\api\SelectorInterface;
use liberty_code_module\error\handler\library\ToolBoxHttpRender;
use liberty_code_module\error\handler\library\ToolBoxCommandRender;



class ErrorThrowableWarnHandler extends FixExecThrowableWarnHandler
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Application mode selector instance.
     * @var SelectorInterface
     */
    protected $objAppMode;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param SelectorInterface $objAppMode
     */
    public function __construct(
        SelectorInterface $objAppMode
    )
    {
        // Init var
        $this->objAppMode = $objAppMode;

        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check render required.
     *
     * @return boolean
     */
    protected function checkRenderRequired()
    {
        // Return result
        return boolval($this->objAppMode->getActiveModeValue(ConstConfig::TAB_CONFIG_KEY_DEBUG));
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstThrowableWarnHandler::TAB_CONFIG_KEY_THROW_CLASS_PATH_INCLUDE => [
                ErrorException::class
            ]
        );
    }



    /**
     * @inheritdoc
     * @param ErrorException[] $tabWarning
     */
    protected function getObjResponse(
        array $tabWarning,
        ResponseInterface $objExecutionResponse,
        $boolExecutionSuccess
    )
    {
        // Init var
        $result = $objExecutionResponse;

        if($result instanceof DefaultResponse)
        {
            // Get render
            $strRender = $result->getContent();
            if(
                (count($tabWarning) > 0) &&
                ($this->checkRenderRequired())
            )
            {
                $strRender .= (
                    ($result instanceof HttpResponse) ?
                        ToolBoxHttpRender::getStrWarnRender($tabWarning, true) :
                        ToolBoxCommandRender::getStrWarnRender($tabWarning, true)
                );
            }

            // Set render
            $result->setContent($strRender . PHP_EOL);
        }

        // Return result
        return $result;
    }



}