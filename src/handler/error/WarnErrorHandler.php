<?php
/**
 * Description :
 * Allows to define warning error handler class.
 * Warning error handler is fixed warning error handler,
 * allows to handle standard errors with level notice, warning, ... (not fatal errors).
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\error\handler\error;

use liberty_code\error\warning\error\handler\error\fix\model\FixWarnErrorHandler;

use liberty_code\error\handler\error\library\ConstErrorHandler;



class WarnErrorHandler extends FixWarnErrorHandler
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstErrorHandler::TAB_CONFIG_KEY_ERROR_TYPE_INCLUDE => [
                E_WARNING,
                E_NOTICE,
                E_CORE_WARNING,
                E_COMPILE_WARNING,
                E_USER_WARNING,
                E_USER_NOTICE,
                E_DEPRECATED,
                E_USER_DEPRECATED
            ]
        );
    }



}