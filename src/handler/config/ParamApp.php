<?php

return array(
    // Error configuration
    // ******************************************************************************

    'error' => [
        'execution' => [
            'handler' => [
                'dir_path' => [
                    dirname(__FILE__) . '/../error'
                ]
            ],
            'warning' => [
                'handler' => [
                    'dir_path' => [
                        dirname(__FILE__) . '/../error/execution/warning'
                    ]
                ]
            ]
        ],

        'system' => [
            'handler' => [
                'dir_path' => [
                    dirname(__FILE__) . '/../error',
                    dirname(__FILE__) . '/../error/system'
                ]
            ]
        ]
    ]
);