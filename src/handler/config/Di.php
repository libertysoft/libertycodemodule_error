<?php

use liberty_code\request_flow\request\api\RequestInterface;
use liberty_code\framework\app_mode\select\api\SelectorInterface;
use liberty_code_module\error\handler\error\WarnErrorHandler;
use liberty_code_module\error\handler\error\execution\warning\ErrorThrowableWarnHandler;
use liberty_code_module\error\handler\error\system\ThrowableHandler;



return array(
    // Error handler services
    // ******************************************************************************

    'error_handler_warning_error' => [
        'source' => WarnErrorHandler::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'error_warning_register']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'error_handler_system_throwable' => [
        'source' => ThrowableHandler::class,
        'argument' => [
            ['type' => 'class', 'value' => SelectorInterface::class],
            ['type' => 'class', 'value' => RequestInterface::class],
            ['type' => 'dependency', 'value' => 'error_warning_register'],
            ['type' => 'dependency', 'value' => 'error_warning_handler_execution_error']
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Error warning handler services
    // ******************************************************************************

    'error_warning_handler_execution_error' => [
        'source' => ErrorThrowableWarnHandler::class,
        'argument' => [
            ['type' => 'class', 'value' => SelectorInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ]
);